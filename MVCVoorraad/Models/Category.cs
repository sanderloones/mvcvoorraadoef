﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCVoorraad.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "{0} is verplicht.")]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255, ErrorMessage = "{0} kan maximum 255 karakters bevatten.")]
        [DisplayName("Naam")]
        public string Naam { get; set; }
    }
}