﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVCVoorraad.Models
{
    public class Component
    {

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is verplicht.")]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255, ErrorMessage = "{0} kan maximum 255 karakters bevatten.")]
        [DisplayName("Naam")]
        public string Naam { get; set; }

        public virtual int CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Category Categorie { get; set; }


        [Column(TypeName = "nvarchar")]
        [MaxLength(255, ErrorMessage = "{0} kan maximum 255 karakters bevatten.")]
        [DisplayName("Datasheet")]
        public string Datasheet { get; set; }

        [Column(TypeName = "int")]
        [DisplayName("Aantal")]
        public int Aantal { get; set; }


        [DisplayName("Aankoopprijs")]
        public double Aankoopprijs { get; set; }


    }
}

