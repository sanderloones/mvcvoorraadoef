﻿using MVCVoorraad.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCVoorraad.DAL
{
    public class VoorraadContext : DbContext
    {
        
        public VoorraadContext(): base("VoorraadContext")

    {}
    public DbSet<Category> Categories { get; set; }
    public DbSet<Component> Components { get; set; }

    
    }
}
