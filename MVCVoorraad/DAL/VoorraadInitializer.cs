﻿using MVCVoorraad.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVCVoorraad.DAL
{
    public class VoorraadInitializer : DropCreateDatabaseIfModelChanges<VoorraadContext>
{
    protected override void Seed(VoorraadContext context) { }
}
}