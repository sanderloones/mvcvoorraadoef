﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCVoorraad.DAL;
using MVCVoorraad.Models;

namespace MVCVoorraad.Controllers
{
    public class ComponentsController : Controller
    {
        private VoorraadContext db = new VoorraadContext();

        // GET: Components
        public ActionResult Index(string sortOrder, string searchString, string selectedCategory)
        {
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.OrderPriceSortParm = sortOrder == "order_price" ? "order_price_desc" : "order_price";
            ViewBag.CountSortParm = sortOrder == "Count" ? "Count_desc" : "Count";
            ViewBag.CategorySortParm = sortOrder == "Category" ? "Category_desc" : "Category";
            ViewBag.DatasheetSortParm = sortOrder == "Datasheet" ? "Datasheet_desc" : "Datasheet";
            var components = from s in db.Components
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                components = components.Where(s => s.Naam.Contains(searchString)
                                       /*|| s.Categorie.Naam.Contains(searchString)*/);
            }
            if (!String.IsNullOrEmpty(selectedCategory))
            {
                components = components.Where(s => s.Categorie.Naam.Contains(selectedCategory));
            }
            switch (sortOrder)
            {
                case "order_price_desc":
                    components = components.OrderByDescending(s => s.Aankoopprijs);
                    break;
                case "order_price":
                    components = components.OrderBy(s => s.Aankoopprijs);
                    break;
                case "Count":
                    components = components.OrderBy(s => s.Aantal);
                    break;
                case "Count_desc":
                    components = components.OrderByDescending(s => s.Aantal);
                    break;
                case "Category_desc":
                    components = components.OrderByDescending(s => s.Categorie.Naam);
                    break;
                case "Category":
                    components = components.OrderBy(s => s.Categorie.Naam);
                    break;
                case "Datasheet_desc":
                    components = components.OrderByDescending(s => s.Datasheet);
                    break;
                case "Datasheet":
                    components = components.OrderBy(s => s.Datasheet);
                    break;
                case "name_desc":
                    components = components.OrderByDescending(s => s.Naam);
                    break;
                default:
                    components = components.OrderBy(s => s.Naam);
                    break;
            }
            return View(components.ToList());
            //var components = db.Components.Include(c => c.Categorie);
            //return View(components.ToList());
        }

        // GET: Components/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Component component = db.Components.Find(id);
            if (component == null)
            {
                return HttpNotFound();
            }
            return View(component);
        }

        // GET: Components/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Naam");
            return View();
        }

        // POST: Components/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "Id,Naam,CategoryId,Datasheet,Aantal,Aankoopprijs")] Component component)
        {
            //(int Id, string Naam, int CategoryId, string Datasheet, string Aantal, string Aankoopprijs")
            if (ModelState.IsValid)
            {
                db.Components.Add(component);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Naam", component.CategoryId);
            return View(component);
        }

        // GET: Components/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Component component = db.Components.Find(id);
            if (component == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Naam", component.CategoryId);
            return View(component);
        }

        // POST: Components/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "Id,Naam,CategoryId,Datasheet,Aantal,Aankoopprijs")] Component component)
        {
            if (ModelState.IsValid)
            {
                db.Entry(component).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryId = new SelectList(db.Categories, "CategoryId", "Naam", component.CategoryId);
            return View(component);
        }

        // GET: Components/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Component component = db.Components.Find(id);
            if (component == null)
            {
                return HttpNotFound();
            }
            return View(component);
        }

        // POST: Components/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            Component component = db.Components.Find(id);
            db.Components.Remove(component);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        [Authorize]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
